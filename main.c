/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_next_line.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: vde-sain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/19 10:14:51 by vde-sain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 12:30:06 by vde-sain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../GNL_list/get_next_line.h"
#include <fcntl.h>
#include <stdio.h>

// ONE LINE MAIN
/*
int			main(int ac, char **av)
  {
  int		fd;
  char	*line;

  ac = 0;
  fd = open(av[1], O_RDONLY);
  get_next_line(fd, &line);
  ft_putendl(line);
  free(line);
  close(fd);
  return (0);
  }
*/
//SOLO FD MAIN
/*
int				main(int ac, char **av)
  {
  int			fd;
  char		*line;

  ac = 0;
  fd = open(av[1], O_RDONLY);
  while (get_next_line(fd, &line) == 1)
  {
  ft_putendl(line);
  free(line);
  }
  free(line);
  close(fd);
  return (0);
  }
*/
//MULTI FD MAIN

int			main(int ac, char **av)
  {
  int		fd1;
  int		fd2;
  char	*line;

  fd1 = open(av[1], O_RDONLY);
  fd2 = open(av[2], O_RDONLY);
  ac = 0;
  while (get_next_line(fd1, &line) == 1)
  {
  ft_putendl(line);
  free(line);
  if (get_next_line(fd2, &line) == 1)
  {
  ft_putendl(line);
  free(line);
  }
  }
  free(line);
  close (fd1);
  close (fd2);
  return (0);
  }

// TEST 40

/*int		main(void)
  {

  char	*line;
  int out;
  int p[2];
  char	*str;
  int len = 50;

  str = (char *)malloc(1000 * 1000);
 *str = '\0';
 while (len--)
 strcat(str, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in leo dignissim, gravida leo id, imperdiet urna. Aliquam magna nunc, maximus quis eleifend et, scelerisque non dolor. Suspendisse augue augue, tempus");
 out = dup(1);
 pipe(p);
 dup2(p[1], 1);

 if (str)
 write(1, str, strlen(str));
 close(p[1]);
 dup2(out, 1);
 get_next_line(p[0], &line);
 printf("%d - %s",strcmp(line, str), str);
 }*/

// TEST 41

/*int			main(void)
  {
  char *line;
  int fd;
  int fd2;
  int fd3;
  int diff_file_size;

  system("mkdir -p ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox");
  system("openssl rand -out ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.txt -base64 $((50 * 1000 * 3/4)) 2> /dev/null");

  fd = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.txt", O_RDONLY);
  fd2 = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.txt.mine", O_CREAT | O_RDWR | O_TRUNC, 0755);

  while (get_next_line(fd, &line) == 1)
  {
  write(fd2, line, strlen(line));
  write(fd2, "\n", 1);
  ft_putendl(line);
  }

  close(fd);
  close(fd2);
  system("diff ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.txt ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.txt.mine > ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.diff");
  fd3 = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/large_file.diff", O_RDONLY);
  printf("%d", diff_file_size = read(fd3, NULL, 10));
  close(fd3);
 }*/

// TEST 42

/*int			main()
{
		char *line;
		int fd;
		int fd2;
		int fd3;
		int diff_file_size;

		system	("mkdir -p ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox");
		system("openssl rand -base64 $((30 * 1000 * 3/4)) | tr -d '\n' | tr -d '\r' > ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt");
		system("echo '\n' >> ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt");

		fd = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt", O_RDONLY);
		fd2 = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt.mine", O_CREAT | O_RDWR | O_TRUNC, 0755);

		while (get_next_line(fd, &line) == 1)
		{
			write(fd2, line, strlen(line));
			write(fd2, "\n", 1);
			ft_putendl(line);
		}
		if (line)
			write(fd2, line, strlen(line));
		close(fd);
		close(fd2);

		system("diff ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.txt.mine > ../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.diff");
		fd3 = open("../42FileChecker/moulitest_42projects/get_next_line_tests/sandbox/one_big_fat_line.diff", O_RDONLY);
		printf("%d", diff_file_size = read(fd3, NULL, 10));
	close(fd3);
}*/
